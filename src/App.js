import './App.css'
import Navbar from './Components/Navbar';
import BannerSection from './Components/Banner';
import AboutUsSection from './Components/AboutUsSection';
import Form from './Components/Form';
function App() {
  return (
      <div >

        <Navbar />
        <BannerSection />
        <Form/>

      </div>
  );
}
export default App;