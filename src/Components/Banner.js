import Banner from '../images/bannerIVF.jpg'

function BannerSection() {
    return (
        <div class="bg-cover bg-fixed" >
            <img src={Banner} alt="banner" class="w-screen h-1/2"/>
        </div>
    );
}

export default BannerSection;