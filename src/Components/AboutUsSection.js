import Team from '../images/team.png';

function AboutUsSection() {
    return (
        <div class="w-full y-10 flex flex-col items-center bg-white rounded-lg border shadow-md md:flex-row md:max-w-xl hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700" >
            <div class="flex flex-col justify-between p-4 leading-normal">
                <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">About Us</h5>
                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
                    Since 2015, IV Future has been dedicated to developing customized software for a
                     wide range of customers, including private individuals, business entities and 
                     software companies. Using advanced IT technology, latest programming software 
                     and a highly competent workforce we thrive to make your daily tasks easier.
                </p>
                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
                    Our team of professionals is driven by values such as integrity, creativity,
                    and genuine passion for their line of work. This makes them tackle each project
                    with bold ideas which has led to fascinating final products for our partners.
                </p>
            </div>
            <img class="object-cover w-full h-full rounded-t-lg md:h-auto md:w-48 md:rounded-none md:rounded-l-lg" 
            src={Team} alt="team"></img>
        </div>
    )
};

export default AboutUsSection;